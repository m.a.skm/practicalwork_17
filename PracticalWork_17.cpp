﻿#include <iostream>
#include <vector>

using namespace std;
class DateClass
{
    int m_day;
    int m_month;
    int m_year;
public:
    void SetDate(int day, int month, int year)
    {
        m_day = day;
        m_month = month;
        m_year = year;
    }
public:
    void Print()
    {
        cout << "Today's date: " << m_day << "." << m_month << "." << m_year 
             << endl;
    }
};

class MyClass : public vector <int>
{
public:
    int LengthVector(vector <int> b)
    {
        return b.size();
    }
};

int main()
{
    DateClass Today{};
    Today.SetDate(9,02,2022);
    Today.Print();
    
    int L = 10;
    vector <int> v (L);
    MyClass Module;
    cout << "Vector length v = " << Module.LengthVector(v) << endl;
    
    return 0;
}
